import GoogleAPIClient
import GTMOAuth2
import UIKit

class DailyChangesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    fileprivate let kKeychainItemName = "Google Sheets API"
    fileprivate let kClientID = "31222056801-d053iug7dut1rastn7am27a3nbkje28p.apps.googleusercontent.com"
    
    // If modifying these scopes, delete your previously saved credentials by
    // resetting the iOS simulator or uninstall the app.
    fileprivate let scopes = ["https://www.googleapis.com/auth/spreadsheets.readonly"]
    
    fileprivate let service = GTLService()
    let output = UITextView()
    var changesArray : [String] = []
    let textCellIdentifier = "TextCell"
    //private var tb: UITableView?
    
    @IBOutlet weak var changesTable: UITableView!
    // When the view loads, create necessary subviews
    // and initialize the Google Sheets API service
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.frame = view.bounds
        output.isEditable = false
        output.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        output.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        //view.addSubview(output);
        
        if let auth = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychain(
            forName: kKeychainItemName,
            clientID: kClientID,
            clientSecret: nil) {
            service.authorizer = auth
        }
        changesTable.delegate = self
        changesTable.dataSource = self
    }
    
    // When the view appears, ensure that the Google Sheets API service is authorized
    // and perform API calls
    override func viewDidAppear(_ animated: Bool) {
        if let authorizer = service.authorizer,
            let canAuth = authorizer.canAuthorize , canAuth {
            listMajors()
        } else {
            present(
                createAuthController(),
                animated: true,
                completion: nil
            )
        }
    }
    
    // Display (in the UITextView) the names and majors of students in a sample
    // spreadsheet:
    // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
    func listMajors() {
        output.text = "Getting sheet data..."
        let baseUrl = "https://sheets.googleapis.com/v4/spreadsheets"
        let spreadsheetId = "13WCEPDBh0KU75APOiP7mHR7Bg7R0KyOPcKjW0FBzDKg"
        let range = "A1:C6"
        let url = String(format:"%@/%@/values/%@", baseUrl, spreadsheetId, range)
        let params = ["majorDimension": "ROWS"]
        let fullUrl = GTLUtilities.url(with: url, queryParameters: params)
        service.fetchObject(with: fullUrl!,
                                   objectClass: GTLObject.self,
                                   delegate: self,
                                   didFinish: #selector(DailyChangesViewController.displayResultWithTicket(_:finishedWithObject:error:))
        )
    }
    
    // Process the response and display output
    func displayResultWithTicket(_ ticket: GTLServiceTicket,
                                 finishedWithObject object : GTLObject,
                                                    error : NSError?) {
        
        if let error = error {
            showAlert("Error", message: error.localizedDescription)
            return
        }
        
        var majorsString = ""
        let rows = object.json["values"] as! [[String]]
        
        if rows.isEmpty {
            output.text = "No data found."
            return
        }
        
        majorsString += "Stunda, Klase, Izmaiņa:\n"
        //changesArray = rows as NSArray;
        for row in rows {
            let stunda = row[0] ?? "Unknown"
            let klase = row[1] ?? "Unknown"
            let izmaina = row[2] ?? "Unknown"
            
            changesArray.append(stunda)
            
            majorsString += "\(stunda), \(klase), \(izmaina)\n"
        }
        
        
//        var error: NSError?
//        if let JSONData = object.json { // Check 1.
//            if let JSONDictionary = JSONSerialization.jsonObject(with: JSONData, options: <#T##JSONSerialization.ReadingOptions#>)
//                println("Dictionary received")
//            }
//            else {
//                
//                if let jsonString = NSString(data: JSONData, encoding: NSUTF8StringEncoding) {
//                    println("JSON: \n\n \(jsonString)")
//                }
//                fatalError("Can't parse JSON \(error)")
//            }
//        }
//        else {
//            fatalError("JSONData is nil")
//        }
//        
        
        output.text = majorsString
        changesTable?.reloadData()
    }
    
    // MARK:  UITextFieldDelegate Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath as IndexPath)
        
        _ = indexPath.row
        
        if (changesArray.count > 3) {
            cell.textLabel?.text =  changesArray[indexPath.row] }
        else {
        cell.textLabel?.text = "Izmaiņas tiek ielādētas"
        }
        
        return cell
    }
    
    // MARK:  UITableViewDelegate Methods
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        _ = indexPath.row
        print("abc")
    }

    // Creates the auth controller for authorizing access to Google Sheets API
    fileprivate func createAuthController() -> GTMOAuth2ViewControllerTouch {
        let scopeString = scopes.joined(separator: " ")
        return GTMOAuth2ViewControllerTouch(
            scope: scopeString,
            clientID: kClientID,
            clientSecret: nil,
            keychainItemName: kKeychainItemName,
            delegate: self,
            finishedSelector: #selector(DailyChangesViewController.viewController(_:finishedWithAuth:error:))
        )
    }
    
    // Handle completion of the authorization process, and update the Google Sheets API
    // with the new credentials.
    func viewController(_ vc : UIViewController,
                        finishedWithAuth authResult : GTMOAuth2Authentication, error : NSError?) {
        
        if let error = error {
            service.authorizer = nil
            showAlert("Authentication Error", message: error.localizedDescription)
            return
        }
        
        service.authorizer = authResult
        dismiss(animated: true, completion: nil)
    }
    
    // Helper for showing an alert
    func showAlert(_ title : String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let ok = UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil
        )
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
